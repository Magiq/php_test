<?php

namespace Controller;
use Framework\Controller;
use Framework\Http\Request;
use Framework\Http\Response;
use Framework\Http\Exception\NotFoundException;
use Model\Task;
use Framework\Exception\ValidationException;

class TaskController extends Controller {

    public function index(Request $request, $page = 1)
    {        
        $task = $this->getModel(Task::class);
        $tasks = $task->getAll($page);
        $count = $task->getTotal();
        
        $total_pages = $count / 3;
        $total_pages = (int) $total_pages;
        if($count % 3 > 0) $total_pages++;

        return new Response($this->render('task/list.tpl', ['tasks' => $tasks, 'total_pages' => $total_pages, 'page' => $page]));
    }

    public function add(Request $request)
    {
        if($request->getMethod() == 'POST') {
            $data = $request->getPost();
            $task = $this->getModel(Task::class);

            if(isset($data['images']) && is_array($data['images'])) {
                foreach($data['images'] as $image) {
                    if(!file_exists(UPLOAD_DIR . DS . basename($image))) {
                        return new Response($this->render('task/add.tpl', ['error' => 'Image not found']));
                    }

                    $images[] = $image;
                }
            }
            
            try {
                $task->save($data);
            } catch (ValidationException $e) {
                return new Response($this->render('task/add.tpl', ['error' => $e->getMessage()]));
            }

            $response = new Response();
            return $response->redirect('/');
        }

        return new Response($this->render('task/add.tpl'));
    }

    public function edit(Request $request, $id) {
        $session = $this->container->get('session');

        if(!$session->get('loggined')) {
            $response = new Response();
            return $response->redirect('/login');
        }

        $taskModel = $this->getModel(Task::class);
        $task = $taskModel->getById($id);

        if(!$task) {
            throw new NotFoundException;
        }

        if($request->getMethod() == 'POST') {
            $data = $request->getPost();
            $data['is_done'] = (isset($data['is_done'])) ? 1 : 0;
            $data['is_consolidated'] = (isset($data['is_consolidated'])) ? 1 : 0;

            $task = $taskModel->save($data, $task->id);
        }

        return new Response($this->render('task/edit.tpl', ['task' => $task]));
    }

    public function preview(Request $request)
    {
        $query = $request->getPost();
        $task = array_intersect_key($query, array_flip(['username', 'email', 'text', 'images']));

        return new Response($this->render('task/show.tpl', ['task' => (object)$task]));
    }
}