<?php

namespace Controller;
use Framework\Controller;
use Framework\Http\Request;
use Framework\Http\JsonResponse;

class ImageController extends Controller
{
    public function upload(Request $request)
    {
        //Todo: check if file exists;

        $handle = new \upload($_FILES['image']);
        if ($handle->uploaded) {
            //Hash to prevent copies
            $handle->file_new_name_body = hash_file('md5', $_FILES['image']['tmp_name']);
            $handle->image_resize = true;
            $handle->image_x = 320;
            $handle->image_y = 240;
            $handle->image_ratio = true;
            $handle->file_overwrite = true;
            $handle->process(UPLOAD_DIR);

            if ($handle->processed) {                        
                $handle->clean();
            } else {
                return new JsonResponse(['status' => 'NOT_OK', 'message' => $handle->error]);
            }
        }

        if(empty($errors)) return new JsonResponse(['status' => 'OK', 'image' => '/' . basename(UPLOAD_DIR) . '/' . $handle->file_dst_name]);
    }
}