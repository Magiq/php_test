<?php

namespace Controller;
use Framework\Http\Response;
use Framework\Http\Request;

class HelloController
{
    public function sayHello(Request $request)
    {
        return new Response('hello');
    }
}