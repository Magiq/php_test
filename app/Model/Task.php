<?php

namespace Model;
use Framework\Exception\ValidationException;
use PDO;
use Framework\Model;

class Task extends Model
{
    static public $fields = ['username', 'email', 'text'];
    static $strip_tags = ['text' => ['<br>', '<b>', '<i>', '<a>', '<ul>', '<li>', '<ol>']];

    private function strip_tags($res) {
        foreach(self::$fields as $key) {
            if(in_array($key, array_keys(self::$strip_tags))) {
                $res->$key = strip_tags($res->$key, join('', self::$strip_tags[$key]));
            } else {
                $res->$key = strip_tags($res->$key);
            }
        }

        return $res;
    }

    public function getById($id)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM tasks WHERE id=?');
        $stmt->execute([$id]);
        $res = $stmt->fetchObject();

        $res = $this->strip_tags($res);

        return $this->load($res);
    }

    public function getTotal()
    {
        $stmt = $this->pdo->prepare('SELECT count(*) FROM tasks');
        $stmt->execute();
        return (int)$stmt->fetchColumn();
    }

    public function getAll($page = 1, $perPage = 3)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM tasks  ORDER BY is_consolidated DESC, id DESC LIMIT ? OFFSET ?');
        $stmt->execute([$perPage, ($page-1) * 3]);

        $result = $stmt->fetchAll(PDO::FETCH_CLASS);
        
        foreach($result as &$res) {
            if($res->images) {
                $res->images = unserialize($res->images);
            }

            $this->strip_tags($res);
        }

        return $result;
    }

    public function save($data, $id = null)
    {
        $this->validate($data);
        $this->loadArray($data);
        
        if($id) {
            $stmt = $this->pdo->prepare('UPDATE tasks SET username = ?, email = ?, text = ?, is_done = ?, is_consolidated = ? WHERE id = ?');
            $stmt->execute([$data['username'], $data['email'], $data['text'], $data['is_done'], $data['is_consolidated'], $id]);            
        } else {
            if($data['images']) {
                $stmt = $this->pdo->prepare('INSERT INTO tasks(username, email, text, images) values(?, ?, ?, ?)');
                $stmt->execute([$data['username'], $data['email'], $data['text'], serialize($data['images'])]);
            } else {
                $stmt = $this->pdo->prepare('INSERT INTO tasks(username, email, text) values(?, ?, ?)');
                $stmt->execute([$data['username'], $data['email'], $data['text']]);
            }
        }

        return $this;
    }

    private function validate($data)
    {
        if(!array_reduce(['username', 'email', 'text'], function($prev, $item) use ($data) {
            return $prev && isset($data[$item]) && !empty($data[$item]);
        }, true)) {                
            throw new ValidationException('Fill all required fields');
        }

        if(isset($data['images']) && is_array($data['images'])) {
            if(!is_array($data['images'])) {
                throw new ValidationException('Not valid images');
            }

            $images = [];
            foreach($data['images'] as $image) {
                if(!file_exists(UPLOAD_DIR . DS . basename($image))) {
                    throw new ValidationException('Image not found');
                }

                $images[] = $image;
            }

            if($images) {
                $data['images'] = serialize($images);
            }
        }

        if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new ValidationException('Email is not valid');
        }
    }
}