<?php

namespace Model;
use Framework\Exception\ValidationException;
use Framework\Model;

class User extends Model
{
    public function login($username, $password)
    {
        $this->validate($username, $password);
        $stmt = $this->pdo->prepare('SELECT * FROM users WHERE username = ? and password = ?');
        $stmt->execute([$username, md5($password)]);
        return $stmt->fetchObject();
    }

    private function validate($username, $password)
    {
        if(!isset($username) || empty($username) || !isset($password) || empty($password)) {
            throw new ValidationException('username and/or password is empty');
        }
    }
}