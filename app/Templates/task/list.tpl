{extends 'layout.tpl'}

{block 'content'}
    <style>
        .task {
            padding-left: 74px;
        }

        .done {
            background: url('/img/tick.png') left 28px no-repeat;
        }
    </style>
    {foreach $tasks as $task}
        <div class="task {if $task->is_done}done{/if}">
            <h3>{$task->username}</h3>
            <p>{$task->text}</p>

            {if $task->images}
                <ul class="list-inline">
                {foreach $task->images as $image}
                    <li>
                        <a href="{$image}" target="_blank"><img src="{$image}" /></a>
                    </li>
                {/foreach}
                </ul>
            {/if}

            <a href="/tasks/edit/{$task->id}">Edit</a>
            <hr />
        </div>
    {/foreach}

    {if $total_pages > 1}
        <nav aria-label="Page navigation">
            <ul class="pagination">
                {if $page > 1}
                    <li>
                        <a href="/tasks/{$page-1}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                {/if}

                {foreach 1..$total_pages as $i}
                    <li><a href="/tasks/{$i}">{$i}</a></li>
                {/foreach}
                {if $page < $total_pages}
                    <li>
                        <a href="/tasks/{$page+1}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                {/if}
            </ul>
        </nav>
    {/if}
{/block}