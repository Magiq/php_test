{extends 'layout.tpl'}

{block 'content'}
    <div id="preview"></div>

    <form method="POST" id="addForm">
        {if isset($error)}
            <div class="alert alert-danger" role="alert">{$error}</div>
        {/if}

        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control" placeholder="username" value="{$task->username}" readonly />
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control" placeholder="email" value="{$task->email}" readonly />
        </div>
        <div id="imagesInput">
        </div>

        <div class="form-group">
            <textarea name="text" class="form-control" rows="3">{$task->text}</textarea>
        </div>

        <div class="form-group">
            <label for="is_done">Выполнено</label>
            <input type="checkbox" name="is_done" {if $task->is_done}checked{/if} />
        </div>

        <div class="form-group">
            <label for="is_consolidated">Закрепить</label>
            <input type="checkbox" name="is_consolidated" {if $task->is_consolidated}checked{/if} />
        </div>

        <button type="submit" class="btn btn-primary">Edit task</button>
    </form>
{/block}