<div class="row">
    <h3>{$task->username}</h3>
    <p>{$task->text}</p>
</div>

<ul class="list-inline">
    {foreach $task->images as $image}
        <li>
            <a href="{$image}" target="_blank"><img src="{$image}" /></a>
        </li>
    {/foreach}
</ul>