{extends 'layout.tpl'}

{block 'content'}
    <style>
        input[type="file"] {
            margin-bottom: 15px;
        }

        ul li{
            position: relative;
        }

        .close{
            display: block;
            float:right;
            position:relative;
            top:2px;
            right: -4px;
        }
    </style>

    <div id="preview"></div>

    <form method="POST" id="addForm">
        {if isset($error)}
            <div class="alert alert-danger" role="alert">{$error}</div>
        {/if}

        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control" placeholder="username" />
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control" placeholder="email" />
        </div>

        <div class="form-group">
            <button id="addImage" class="btn btn-default">Add image</button>
        </div>

        <ul id="imagePreview" class="list-inline">
        </ul>

        <div id="imagesInput">
        </div>

        <div class="form-group">
            <textarea name="text" class="form-control" rows="3"></textarea>
        </div>

        <div class="form-group">
            <button type="button" id="previewButton" class="btn btn-primary">Preview task</button>
            <button type="submit" class="btn btn-primary">Add task</button>
        </div>
    </form>
{/block}

{block 'javascript'}
    {parent}
    <script type="text/javascript" src="/js/add_task.js"></script>
{/block}