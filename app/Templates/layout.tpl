<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.min.css">
        <title>Your personal task manager {if isset($title)}{$title}{/if}</title>
    </head>
    <body>
        {include 'navbar.tpl'}

        <div class="container" style="width: 840px">
            {block 'content'}{/block}
        </div>
    
        {block 'javascript'}
            <script type="text/javascript" src="/vendor/jquery/dist/jquery.min.js"></script>
            <script type="text/javascript" src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        {/block}
    </body>
</html>