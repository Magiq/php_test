{extends 'layout.tpl'}

{block 'content'}
    <form method="post">
        {if isset($error)}
            <div class="alert alert-danger" role="alert">{$error}</div>
        {/if}
        <div class="form-group">
            <label for="username" class="control-label">Username</label>
            <input type="text" id="username" name="username" required="required" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="password" class="control-label">Password</label>
            <input type="password" id="password" name="password" required="required" class="form-control"/>
        </div>

        <input type="submit" id="_submit" name="submit" value="Login" class="btn btn-block"/>
    </form>
{/block}