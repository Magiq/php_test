<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
        <a class="navbar-brand" href="#">Task manager</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tasks
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/tasks/add">Add</a></li>
                    <li><a href="/">List</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>