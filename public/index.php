<?php

require __DIR__ . '/../vendor/autoload.php';

use Framework\Http\Request;

$kernel = new AppKernel(true);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();