<?php

namespace Framework;

use Framework\Route\Router;
use Framework\Http\ResponseInterface;
use Framework\Http\Request;
use Framework\ErrorHandler;
use Framework\Route\Exception\RouteNotFoundException;
use Framework\Http\Exception\NotFoundException;
use Framework\ServiceContainer;
use Framework\Http\Session;

/**
 * Main class
 * 
 */
abstract class Kernel
{

    private $debug;
    private $router;
    private $container;

    /**
     * Kernel constructor
     * 
     * @param boolean $debug
     * @return void
     */
    public function __construct($debug = false, $router = null) {
        $this->debug = $debug;


        if($router instanceof Router) {
            $this->router = $router;
        }

        $this->boot();
    }

    /**
     * Initialization
     * 
     * @return void
     */
    private function boot() {
        if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

        if(!defined('CONFIG_DIR')) define('CONFIG_DIR', $this->getAppDir() . DS . 'configs');

        if(!defined('TEMPLATE_DIR')) define('TEMPLATE_DIR', $this->getAppDir() . DS . 'Templates');

        if(!defined('TMP_DIR')) define('TMP_DIR', $this->getAppDir() . DS . '..' . DS . 'tmp');

        if(!defined('UPLOAD_DIR')) define('UPLOAD_DIR', $this->getAppDir() . DS . '..' . DS . 'public' . DS . 'upload');

        if(!defined('MODEL_DIR')) define('MODEL_DIR', $this->getAppDir() . DS . 'Model');

        if(!defined('DEBUG')) define('DEBUG', $this->debug);        

        ErrorHandler::register();

        $this->container = new ServiceContainer();
        $this->container->instance('router', $this->router);
        $this->container->bind('db', [$this, 'initDb']);

        $fenom = new \Fenom(new \Fenom\Provider(TEMPLATE_DIR));
        $fenom->setCompileDir(TMP_DIR);

        $this->container->instance('template', $fenom);

        $this->container->bind('session', function() {
            return new Session();
        });

        if(!$this->router)
            $this->loadRouter();
    }

    public function initDb()
    {
        $database_config_path = CONFIG_DIR . DS . 'database.php';

        if(!file_exists($database_config_path)) {
            throw new \Exception(sprintf('Can\'t find database config file in %s', $database_config_path));
        }

        $database_config = include($database_config_path);

        if(!array_key_exists('dsn', $database_config)) {
            throw \Exception(sprintf('Specify dsn option in %s', $database_config_path));
        }
        $pdo = new \PDO($database_config['dsn']);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }


    private function loadRouter()
    {
        $routes_path = CONFIG_DIR . DS . 'routes.php';

        if(!file_exists($routes_path)) {
            throw new \Exception(sprintf('Can\'t find router file in %s', $routes_path));
        }

        $router = include($routes_path);

        if(!($router instanceof Router)) {
            throw new \Exception(sprintf('Wrong router type, expecting %s got %s', Router::class, gettype($router)));
        }
        
        $this->router = $router;
    }

    /**
     * Handles request
     * 
     * @param Request $request
     * @return ResponseIterface 
     */
    public function handle(Request $request) {
        try {
            $route = $this->router->match($request);
        } catch(RouteNotFoundException $e) {
            throw new NotFoundException;
        }

        $controller = $route->getController();
        $params = array_merge([$request], $route->getParameters());

        if(is_array($controller)) {
            if(count($controller) !== 2) {
                throw new \Exception(sprintf('Unexpected controller formats %s', var_export($controller, true)));
            }

            list($controller_class, $method_name) = $controller;

            $instance = new $controller_class();
            $instance->setContainer($this->container);
            $response = call_user_func_array([$instance, $method_name], $params);
        }elseif(is_callable($controller)) {
            $response = call_user_func($controller, $request);
        }elseif(is_string($controller)) {
            $controller_parts = explode(":", $controller);

            if(count($controller_parts) !== 2) {
                throw new \Exception(sprintf('Wrong controller format %s', $controller));
            }

            list($controller_class, $method_name) = $controller_parts;

            if(!class_exists($controller_class)) {
                $controller_class = 'Controller\\' . $controller_class;
            }

            $instance = new $controller_class();
            $instance->setContainer($this->container);
            $response = call_user_func_array([$instance, $method_name], $params);
        }else{
            throw new \Exception(sprintf('Wront controller type %s', gettype($controller)));
        }

        return $response;
    }

    /**
     * Returns app directory
     * 
     * @return string
     */
    abstract protected function getAppDir();
}