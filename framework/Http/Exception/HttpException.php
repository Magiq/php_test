<?php

namespace Framework\Http\Exception;

use Framework\Http\Response;

abstract class HttpException extends \Exception
{
    abstract public function getStatusCode();
    abstract public function getTypical();
}