<?php

namespace Framework\Http;
use Framework\Http\ResponseInterface;

class JsonResponse implements ResponseInterface
{

    private $content;
    private $status_code;

    public function __construct($content, $status_code = 200)
    {
        $this->content = $content;
        $this->status_code = $status_code;
    }

    public function send()
    {
        header('Content-Type: application/json');
        http_response_code($this->status_code);
        echo json_encode($this->content);
    }
}