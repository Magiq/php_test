<?php

namespace Framework\Http;
use Framework\Http\ResponseInterface;

class Response implements ResponseInterface
{

    private $content;
    private $status_code;


    public function __construct($content = '', $status_code = 200)
    {
        $this->content = $content;
        $this->status_code = $status_code;
    }

    public function send()
    {
        http_response_code($this->status_code);
        echo $this->content;
    }

    public function redirect($url)
    {
        header('Location: ' . $url, true, 302);
        exit;
    }
}