<?php

namespace Framework\Http;

class Request {

    private $_server;
    private $_get;
    private $_post;
    private $_cookie;
    private $_files;

    public function __construct($_server, $_get = [], $_post = [], $_cookie = [], $_files = []) {
        $this->_server = $_server;
        $this->_get = $_get;
        $this->_post = $_post;
        $this->_cookie = $_cookie;
        $this->_files = $_files;
    }

    static function createFromGlobals() {
        return new Request($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);
    }

    public function getPath() {
        if(!array_key_exists('REQUEST_URI', $this->_server)) {
            throw new \Exception('REQUEST_URI is missing');
        }

        $path = rtrim(parse_url($this->_server['REQUEST_URI'], PHP_URL_PATH), '/'); 
    
        return ($path) ? $path : '_index';
    }

    public function getMethod() {
        if(!array_key_exists('REQUEST_METHOD', $this->_server)) {
            throw new \Exception('REQUEST_METHOD is missing');
        }

        return $this->_server['REQUEST_METHOD'];
    }

    public function getQuery() {
        return $this->_get;
    }

    public function getPost() {
        return $this->_post;
    }

    public function getCookies() {
        return $this->_cookie;
    }

    public function getFiles() {
        return $this->_files;
    }
}