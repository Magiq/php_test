<?php

namespace Framework;

abstract class Model
{
    public $id;
    protected $pdo;
    private $fields;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function __get($name)
    {
        return $this->fields[$name];
    }

    public function __set($name, $value)
    {
        $this->fields[$name] = $value;
    }

    protected function load($object)
    {   
        foreach (get_object_vars($object) as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }

    protected function loadArray($array)
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }
}