<?php

namespace Framework\Route;

class Route
{
    private $path;
    private $controller;
    private $parameters = [];

    public function __construct($path, $controller) {
        $this->path = $path;
        $this->controller = $controller;
    }

    public function getPath() {
        return $this->path;
    }

    public function addParameter($name, $value) {
        $this->parameters[$name] = $value;

        return $this;
    }

    public function getParameters() {
        return $this->parameters;
    }

    public function getController() {
        return $this->controller;
    }
}