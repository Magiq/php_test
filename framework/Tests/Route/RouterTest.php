<?php

namespace Framework\Tests;

use PHPUnit\Framework\TestCase;
use Framework\Http\Request;
use Framework\Route\Route;
use Framework\Route\Router;

class RouterTest extends TestCase
{
    public function testBasicRoutes()
    {
        $tests = [
            '/path' => new Route('/path', 'DefaultController:action1'),
            '/some/' => new Route('/some', 'AnotherController:action2')
        ];

        $this->makeTests($tests);
    }

    public function testPatternRoutes()
    {
        $tests = [
            '/path/4' => new Route('/path/{somenumber}', 'DefaultController:action1', ['somenumber' => '4']),
            '/some/phrase' => new Route('/some/{somestring}', 'AnotherController:action2', ['somestring' => 'phrase'])
        ];

        $this->makeTests($tests);
    }

    private function makeTests($tests)
    {
        $router = new Router;

        foreach($tests as $uri=>$route) {
            $router->add($route);
        }

        foreach($tests as $uri=>$route) {
            $request = new Request(['REQUEST_URI' => $uri]);
            $this->assertEquals($route, $router->match($request));
        } 
    }
}