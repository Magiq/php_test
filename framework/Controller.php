<?php

namespace Framework;

class Controller
{
    protected $container;

    public function setContainer($container) {
        $this->container = $container;
    }

    public function getModel($name) {
        return new $name($this->container->get('db'));
    }

    public function getRouter() {
        return $this->container->get('router');
    }

    public function render($template, $params = []) {
        return $this->container->get('template')->fetch($template, $params);
    }
}